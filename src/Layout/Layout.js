import React from "react";
import Header from "../Component/Header/Header";
import HomePage from "../Page/HomePage/HomePage";

export default function Layout({ Component }) {
  return (
    <div className="space-y-10">
      <Header />
      <Component />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}
