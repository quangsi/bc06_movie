import React from "react";
import Header from "../../Component/Header/Header";
import ListMovie from "./ListMovie/ListMovie";
import TabMovie from "./TabMovie/TabMovie";

export default function HomePage() {
  return (
    <div>
      {/* <Header /> */}
      <ListMovie />
      <TabMovie />
    </div>
  );
}
