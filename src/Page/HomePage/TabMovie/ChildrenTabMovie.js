import moment from "moment";
import React from "react";

export default function ChildrenTabMovie({ listMovie }) {
  return (
    <div style={{ height: 600 }} className="overflow-y-auto">
      {listMovie.map((phim) => {
        return (
          <div className="flex">
            <img className="w-24" src={phim.hinhAnh} alt="" />
            <div>
              <p>{phim.tenPhim}</p>
              <div>
                {phim.lstLichChieuTheoPhim.map((lich) => {
                  return (
                    <p>{moment(lich.ngayChieuGioChieu).format("DD-MM-YYYY")}</p>
                  );
                })}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}
