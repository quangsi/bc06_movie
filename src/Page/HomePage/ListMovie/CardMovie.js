import { Card } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function CardMovie({ movie }) {
  let { tenPhim, hinhAnh } = movie;
  return (
    <Card
      hoverable
      style={{
        width: "100%",
      }}
      cover={
        <img
          style={{ height: 200, objectFit: "cover", objectPosition: "top" }}
          alt="example"
          src={hinhAnh}
        />
      }
    >
      {/* xem chi tiet */}
      <NavLink className={"text-red-500"} to={`/detail/${movie.maPhim}`}>
        Xem ngay
      </NavLink>
      <Meta title={tenPhim} />
    </Card>
  );
}
