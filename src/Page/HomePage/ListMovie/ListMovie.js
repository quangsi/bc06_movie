import React, { useEffect } from "react";
import { useState } from "react";
import { movieServ } from "../../../services/movieServices";
import CardMovie from "./CardMovie";

export default function ListMovie() {
  const [movies, setMovies] = useState();
  useEffect(() => {
    movieServ
      .getMovieList()
      .then((res) => {
        console.log(res.data.content);
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="grid grid-cols-5 gap-10 container mx-auto">
      {/* grid col 5 tailwind + card: hinhAnh,tenPhim */}
      {/* optional chaining */}
      {movies?.map((item) => {
        return <CardMovie movie={item} />;
      })}
    </div>
  );
}
